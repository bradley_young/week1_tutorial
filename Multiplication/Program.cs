﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class MultiplicationTable
{
	public static void Main()
	{
		int a, b;

		Console.Write("\n\n");
		Console.Write("Display the multiplication table:\n");
		Console.Write("-----------------------------------");
		Console.Write("\n\n");

		Console.Write("Input the number (Table to be calculated) : ");
		b = Convert.ToInt32(Console.ReadLine());
		Console.Write("\n");
		for (a = 1; a <= 10; a++)
		{
			Console.Write("{0} X {1} = {2} \n", b, a, b * a);
		}
		Console.ReadLine();
	}
}