﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Circle
{
	public static void Main()
	{

		double r, perimeter, area;
		double PI = 3.14;
		Console.WriteLine("Input the radius of the circle : ");
		r = Convert.ToDouble(Console.ReadLine());
		perimeter = 2 * PI * r;
		area = PI * r * r;
		Console.WriteLine("Perimeter of Circle : {0}", perimeter);
		Console.WriteLine("Area of Circle : {0}", area);
		Console.Read();
		

	}
}