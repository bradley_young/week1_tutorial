﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biggest
{
	class Program
	{
		static void Main(string[] args)
		{
			int num1, num2, num3;
			Console.Write("\n\n");
			Console.Write("Find the largest number out of 3 options:\n");
			Console.Write("------------------------------------------");
			Console.Write("\n\n");
			Console.Write("Input the first number: ");
			num1 = Convert.ToInt32(Console.ReadLine());
			Console.Write("Input the second number: ");
			num2 = Convert.ToInt32(Console.ReadLine());
			Console.Write("Input the third  number: ");
			num3 = Convert.ToInt32(Console.ReadLine());

			if (num1 > num2)
			{
				if (num1 > num3)
				{
					Console.Write("The first number is the greatest among the three. \n\n");
				}
				else
				{
					Console.Write("The third number is the greatest among the three. \n\n");
				}
			}
			else if (num2 > num3)
				Console.Write("The second number is the greatest among the three \n\n");
			else
				Console.Write("The third number is the greatest among the three \n\n");
			Console.ReadLine();
		}
	}
}

